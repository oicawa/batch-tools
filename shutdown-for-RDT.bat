@echo off

:INPUT_OPTION

echo.
echo Select a number what you want in following.
echo.
echo [1] Shutdown (Power Off)
echo [2] Reboot
echo [3] Cancel
echo.

set /p MODE=" >> "

if "%MODE%"=="1" (
  echo Start shutdown...
  shutdown -s
) else if "%MODE%"=="2" (
  echo Start Reboot...
  shutdown -r -t 0
) else if "%MODE%"=="3" (
  echo Canceled. Bye.
) else (
  echo Invalid character. Please input [1] or [2] or [3] only.
  goto INPUT_OPTION
)

pause